module decode(clk, insn, addr, d_in, d_dst, we,
              rsOut, rtOut, jmp_tgt, Rdst, Rwe, jmp, stall, mx_bypass, wx_bypass);
  input        clk;
  input [0:31] insn;
  input [0:31] addr;
  input [0:31] d_in;
  input [0:4]  d_dst;
  input        we;

  output [0:31] rsOut;
  output [0:31] rtOut;
  output [0:31] jmp_tgt;
  output        stall;
  output        jmp;

  output reg [0:31] Rdst;
  output reg        Rwe;
  output reg [0:1]  mx_bypass;
  output reg [0:1]  wx_bypass;

  wire [0:5] opcode;
  wire [0:4] rs;
  wire [0:4] rt;
  wire [0:4] rd;
  wire [0:4] shamt;
  wire [0:5] func;
  wire [0:15] immed;
  wire [0:25] target;

  reg [0:31] lastInsn;
  reg [0:4] lastRd;
  reg [0:4] secondLastRd;

  assign opcode = insn[0:5];
  assign rs     = insn[6:10];
  assign rt     = insn[11:15];
  assign rd     = insn[16:20];
  assign shamt  = insn[21:25];
  assign func   = insn[26:31];
  assign immed  = insn[16:31];
  assign target = insn[6:31];

  assign stall = ((lastRd == rs || lastRd == rt) // If the last destination is being used as a paramter for current insn
                  // And the last instruction is any of the loads
                  && (lastInsn[0:5] == 6'b100000 || lastInsn[0:5] == 6'b100001
                      || lastInsn[0:5] == 6'b100011 || lastInsn[0:5] == 6'b100100)) ? 1 : 0;

  assign jmp_tgt[0:3]   = addr[0:3];
  assign jmp_tgt[4:29]  = target;
  assign jmp_tgt[30:31] = 2'b00;

  assign jmp = (opcode == 6'b000010 || opcode == 6'b000011) ? 1 : 0;

  register regfile(clk, we, d_in, rs, rt, d_dst, rsOut, rtOut);

  always @ (posedge clk)
  begin: BYPASS_CONTROL
    if (stall === 1) begin
      lastInsn <= 32'h0;
    end else begin
      lastInsn <= insn;
    end
    secondLastRd <= lastRd;

    if (lastRd == rs) begin
      mx_bypass <= 1;
    end else if (lastRd == rt) begin
      mx_bypass <= 2;
    end else begin
      mx_bypass <= 0;
    end

    if (secondLastRd == rs) begin
      wx_bypass <= 1;
    end else if (secondLastRd == rt) begin
      wx_bypass <= 2;
    end else begin
      wx_bypass <= 0;
    end
  end

  always @ (posedge clk)
  begin: INSTRUCTION_PARSING
    if (stall === 1) begin
      lastRd <= 5'hx;
    end else begin
    $display("Output from RF: %h (rs) and %h (rt)", rsOut, rtOut);
    case (opcode)
      // R-types
      6'b000000: case (func)
        6'b000000: begin
          if (insn == 32'b0) begin
            $display("0x%h: %h NOP", addr, insn);
             lastRd <= 5'hx;
          end else begin
            $display("0x%h: %h SLL   $%0d, $%0d, %0d", addr, insn, rd, rt, shamt);
            lastRd <= rd;
          end
          Rwe  <= 1;
          Rdst <= rd;
        end
        6'b000010: begin
          $display("0x%h: %h SRL   $%0d, $%0d, %0d", addr, insn, rd, rt, shamt);
          Rwe  <= 1;
          Rdst <= rd;
          lastRd <= rd;
        end
        6'b000011: begin
          $display("0x%h: %h SRA   $%0d, $%0d, %0d", addr, insn, rd, rt, shamt);
          Rwe  <= 1;
          Rdst <= rd;
          lastRd <= rd;
        end
        6'b000100: begin
          $display("0x%h: %h SLLV  $%0d, $%0d, $%0d", addr, insn, rd, rt, rs);
          Rwe  <= 1;
          Rdst <= rd;
          lastRd <= rd;
        end
        6'b000110: begin
          $display("0x%h: %h SRLV  $%0d, $%0d, $%0d", addr, insn, rd, rt, rs);
          Rwe  <= 1;
          Rdst <= rd;
          lastRd <= rd;
        end
        6'b000111: begin
          $display("0x%h: %h SRAV  $%0d, $%0d, $%0d", addr, insn, rd, rt, rs);
          Rwe  <= 1;
          Rdst <= rd;
          lastRd <= rd;
        end
        6'b001000: begin
          $display("0x%h: %h JR    $%0d", addr, insn, rs);
          Rwe  <= 0;
          lastRd <= 5'hx;
        end
        6'b001001: begin
          $display("0x%h: %h JALR  $%0d, $%0d", addr, insn, rd, rs);
          Rwe  <= 1;
          Rdst <= rd;
          lastRd <= rd;
        end
        6'b001010: begin
          $display("0x%h: %h MOVZ  $%0d, $%0d, $%0d", addr, insn, rd, rs, rt);
          Rwe  <= 1;
          Rdst <= rd;
          lastRd <= rd;
        end
        6'b001011: begin
          $display("0x%h: %h MOVN  $%0d, $%0d, $%0d", addr, insn, rd, rs, rt);
          Rwe  <= 1;
          Rdst <= rd;
          lastRd <= rd;
        end
        6'b010000: begin
          $display("0x%h: %h MFHI  $%0d", addr, insn, rd);
          Rwe  <= 1;
          Rdst <= rd;
          lastRd <= rd;
        end
        6'b010010: begin
          $display("0x%h: %h MFLO  $%0d", addr, insn, rd);
          Rwe  <= 1;
          Rdst <= rd;
          lastRd <= rd;
        end
        6'b011010: begin
          $display("0x%h: %h DIV   $%0d, $%0d", addr, insn, rs, rt);
          Rwe  <= 0;
          Rdst <= rs;
          lastRd <= rs;
        end
        6'b011011: begin
          $display("0x%h: %h DIVU  $%0d, $%0d", addr, insn, rs, rt);
          Rwe  <= 0;
          Rdst <= rs;
          lastRd <= rs;
        end
        6'b100000: begin
          if (rt == 6'b0 && rs == 6'b0) begin
            $display("0x%h: %h CLEAR $%0d", addr, insn, rd);
          end else if (rt == 6'b0) begin
            $display("0x%h: %h MOVE  $%0d, $%0d", addr, insn, rd, rs);
          end else begin
            $display("0x%h: %h ADD   $%0d, $%0d, $%0d", addr, insn, rd, rs, rt);
          end
          Rwe  <= 1;
          Rdst <= rd;
          lastRd <= rd;
        end
        6'b100001: begin
          if (rt == 6'b0 && rs == 6'b0) begin
            $display("0x%h: %h CLEAR $%0d", addr, insn, rd);
          end else if (rt == 6'b0) begin
            $display("0x%h: %h MOVE  $%0d, $%0d", addr, insn, rd, rs);
          end else begin
            $display("0x%h: %h ADDU  $%0d, $%0d, $%0d", addr, insn, rd, rs, rt);
          end
          Rwe  <= 1;
          Rdst <= rd;
          lastRd <= rd;
        end
        6'b100010: begin
          $display("0x%h: %h SUB   $%0d, $%0d, $%0d", addr, insn, rd, rs, rt);
          Rwe  <= 1;
          Rdst <= rd;
          lastRd <= rd;
        end
        6'b100011: begin
          $display("0x%h: %h SUBU  $%0d, $%0d, $%0d", addr, insn, rd, rs, rt);
          Rwe  <= 1;
          Rdst <= rd;
          lastRd <= rd;
        end
        6'b100100: begin
          $display("0x%h: %h AND   $%0d, $%0d, $%0d", addr, insn, rd, rs, rt);
          Rwe  <= 1;
          Rdst <= rd;
          lastRd <= rd;
        end
        6'b100101: begin
          $display("0x%h: %h OR    $%0d, $%0d, $%0d", addr, insn, rd, rs, rt);
          Rwe  <= 1;
          Rdst <= rd;
          lastRd <= rd;
        end
        6'b100110: begin
          $display("0x%h: %h XOR   $%0d, $%0d, $%0d", addr, insn, rd, rs, rt);
          Rwe  <= 1;
          Rdst <= rd;
          lastRd <= rd;
        end
        6'b100111: begin
          if (rt == 5'b0) begin
            $display("0x%h: %h NOT   $%0d, $%0d", addr, insn, rd, rs);
          end else begin
            $display("0x%h: %h NOR   $%0d, $%0d, $%0d", addr, insn, rd, rs, rt);
          end
          Rwe  <= 1;
          Rdst <= rd;
          lastRd <= rd;
        end
        6'b101010: begin
          $display("0x%h: %h SLT   $%0d, $%0d, $%0d", addr, insn, rd, rs, rt);
          Rwe  <= 1;
          Rdst <= rd;
          lastRd <= rd;
        end
        6'b101011: begin
          $display("0x%h: %h SLTU  $%0d, $%0d, $%0d", addr, insn, rd, rs, rt);
          Rwe  <= 1;
          Rdst <= rd;
          lastRd <= rd;
        end
      endcase
      // Special case I-types
      6'b000001: case (rt)
        6'b00000: begin
          $display("0x%h: %h BLTZ  $%d, 0x%h", addr, insn, rs, (immed << 2));
          Rwe  <= 0;
          lastRd <= 5'hx;
        end
        6'b00001: begin
          $display("0x%h: %h BGEZ  $%d, 0x%h", addr, insn, rs, (immed << 2));
          Rwe  <= 0;
          lastRd <= 5'hx;
        end
      endcase
      // J-types
      6'b000010: begin
        $display("0x%h: %h J     0x%h%h", addr, insn, addr[0:3], (target << 2));
        Rwe <= 0;
        lastRd <= 5'hx;
      end
      6'b000011: begin
        $display("0x%h: %h JAL   0x%h%h", addr, insn, addr[0:3], (target << 2));
        Rwe  <= 1;
        Rdst <= 5'h1f;
        lastRd <= 5'hx;
      end
      // I-types
      6'b000100: begin
        if (rs == 5'b0 && rt == 5'b0) begin
          $display("0x%h: %h B     $%0d", addr, insn, (immed << 2));
        end else if (rt == 5'b0) begin
          $display("0x%h: %h BEQZ  $%0d, 0x%h", addr, insn, rs, (immed << 2));
        end else begin
          $display("0x%h: %h BEQ   $%0d, $%0d, 0x%h", addr, insn, rs, rt, (immed << 2));
        end
        Rwe <= 0;
        lastRd <= 5'hx;
      end
      6'b000101: begin
        $display("0x%h: %h BNE   $%0d, $%0d, 0x%h", addr, insn, rs, rt, (immed << 2));
        Rwe <= 0;
        lastRd <= 5'hx;
      end
      6'b000110: begin
        $display("0x%h: %h BLEZ  $%0d, 0x%h", addr, insn, rs, (immed << 2));
        Rwe <= 0;
        lastRd <= 5'hx;
      end
      6'b000111: begin
        $display("0x%h: %h BGTZ  $%0d, 0x%h", addr, insn, rs, (immed << 2));
        Rwe <= 0;
        lastRd <= 5'hx;
      end
      6'b001000: begin
        $display("0x%h: %h ADDI  $%0d, $%0d, %0d", addr, insn, rt, rs, immed);
        Rwe  <= 1;
        Rdst <= rt;
        lastRd <= rt;
      end
      6'b001001: begin
        $display("0x%h: %h ADDIU $%0d, $%0d, %0d", addr, insn, rt, rs, immed);
        Rwe  <= 1;
        Rdst <= rt;
        lastRd <= rt;
      end
      6'b001010: begin
        $display("0x%h: %h SLTI  $%0d, $%0d, %0d", addr, insn, rt, rs, immed);
        Rwe  <= 1;
        Rdst <= rt;
        lastRd <= rt;
      end
      6'b001011: begin
        $display("0x%h: %h SLTIU $%0d, $%0d, %0d", addr, insn, rt, rs, immed);
        Rwe  <= 1;
        Rdst <= rt;
      end
      6'b001100: begin
        $display("0x%h: %h ANDI  $%0d, $%0d, %0d", addr, insn, rt, rs, immed);
        Rwe  <= 1;
        Rdst <= rt;
        lastRd <= rt;
      end
      6'b001101: begin
        $display("0x%h: %h ORI   $%0d, $%0d, %0d", addr, insn, rt, rs, immed);
        Rwe  <= 1;
        Rdst <= rt;
        lastRd <= rt;
      end
      6'b001110: begin
        $display("0x%h: %h XORI  $%0d, $%0d, %0d", addr, insn, rt, rs, immed);
        Rwe  <= 1;
        Rdst <= rt;
        lastRd <= rt;
      end
      6'b001111: begin
        $display("0x%h: %h LUI   $%0d, $%h", addr, insn, rt, (immed << 16));
        Rwe  <= 1;
        Rdst <= rt;
        lastRd <= rt;
      end
      6'b011100: case (func)
        6'b000010: begin
          $display("0x%h: %h MUL   $%0d, $%0d, %0d", addr, insn, rd, rs, rt);
          Rwe  <= 1;
          Rdst <= rd;
          lastRd <= rd;
        end
      endcase
      6'b100000: begin
        $display("0x%h: %h LB    $%0d, %0d($%0d)", addr, insn, rt, immed, rs);
        Rwe  <= 1;
        Rdst <= rt;
        lastRd <= rt;
      end
      6'b100001: begin
        $display("0x%h: %h LH    $%0d, %0d($%0d)", addr, insn, rt, immed, rs);
        Rwe  <= 1;
        Rdst <= rt;
        lastRd <= rt;
      end
      6'b100011: begin
        $display("0x%h: %h LW    $%0d, %0d($%0d)", addr, insn, rt, immed, rs);
        Rwe  <= 1;
        Rdst <= rt;
        lastRd <= rt;
      end
      6'b100100: begin
        $display("0x%h: %h LBU   $%0d, %0d($%0d)", addr, insn, rt, immed, rs);
        Rwe  <= 1;
        Rdst <= rt;
        lastRd <= rt;
      end
      6'b101000: begin
        $display("0x%h: %h SB    $%0d, %0d($%0d)", addr, insn, rt, immed, rs);
        Rwe  <= 0;
        lastRd <= 5'hx;
      end
      6'b101001: begin
        $display("0x%h: %h SH    $%0d, %0d($%0d)", addr, insn, rt, immed, rs);
        Rwe  <= 0;
        lastRd <= 5'hx;
      end
      6'b101011: begin
        $display("0x%h: %h SW    $%0d, %0d($%0d)", addr, insn, rt, immed, rs);
        Rwe  <= 0;
        lastRd <= 5'hx;
      end
    endcase
    end
  end
endmodule
