module execute(clk, dx_rs, dx_rt, pc, insn, mx_val, wx_val,
               mx_bypass, wx_bypass,
               rtOut, result, addr, size, DMwe, Rwd, br);
  input        clk;
  input [0:31] dx_rs;
  input [0:31] dx_rt;
  input [0:31] pc;
  input [0:31] insn;
  input [0:31] mx_val;
  input [0:31] wx_val;

  input [0:1]  mx_bypass;
  input [0:1]  wx_bypass;

  output reg [0:31] rtOut;
  output reg [0:31] result;
  output reg [0:31] addr;
  output reg [0:1]  size;
  output reg        DMwe;
  output reg        Rwd;
  output reg        br;

  wire [0:31] rs;
  wire [0:31] rt;

  wire [0:5] opcode;
  wire [0:4] shamt;
  wire [0:5] func;
  wire [0:15] immed;
  wire [0:25] target;

  reg signed [0:31] signed_ext;
  reg [0:31] temp;

  reg [0:31] LO;
  reg [0:31] HI;

  assign opcode = insn[0:5];
  assign shamt  = insn[21:25];
  assign func   = insn[26:31];
  assign immed  = insn[16:31];
  assign target = insn[6:31];

  assign rs = (mx_bypass === 1) ? mx_val : ((wx_bypass === 1) ? wx_val : dx_rs);
  assign rt = (mx_bypass === 2) ? mx_val : ((wx_bypass === 2) ? wx_val : dx_rt);

  always @ (posedge clk)
  begin: OUTPUT_RT
    rtOut <= rt;
  end

  always @(insn)
  begin: EXECUTE_INSN
    case (opcode)
      // R-types
      6'b000000: case (func)
        6'b000000: begin
          $display("Inputs: %0d, shift by %0d", rt, shamt);
          result = rt << shamt;
          DMwe = 0;
          size = 0;
          Rwd  = 1;
          br = 0;
        end
        6'b000010: begin
          $display("Inputs: %0d, shift by %0d", rt, shamt);
          result = rt >> shamt;
          DMwe = 0;
          size = 0;
          Rwd  = 1;
          br = 0;
        end
        6'b000011: begin
          $display("Inputs: %0d, shift by %0d", rt, shamt);
          temp = rt >> shamt;
          result = {rt[0],temp[1:31]};
          DMwe = 0;
          size = 0;
          Rwd  = 1;
          br = 0;
        end
        6'b000100: begin
          $display("Inputs: %0d, shift by %0d", rt, rs[27:31]);
          result = rt << rs[27:31];
          DMwe = 0;
          size = 0;
          Rwd  = 1;
          br = 0;
        end
        6'b000110: begin
          $display("Inputs: %0d, shift by %0d", rt, rs[27:31]);
          result = rt >> rs[27:31];
          DMwe = 0;
          size = 0;
          Rwd  = 1;
          br = 0;
        end
        6'b000111: begin
          $display("Inputs: %0d, shift by %0d", rt, rs[27:31]);
          temp = rt >> rs[27:31];
          result = {rt[0],temp[1:31]};
          DMwe = 0;
          size = 0;
          Rwd  = 1;
          br = 0;
        end
        6'b001000: begin
          $display("Inputs: %0d (target location)", rs);
          addr = rs;
          DMwe = 0;
          size = 0;
          br = 1;
        end
        6'b001001: begin
          $display("Inputs: %0d (target location)", rs);
          result = pc + 8;
          addr = rs;
          DMwe = 0;
          Rwd  = 1;
          size = 0;
          br = 1;
        end
        6'b001010: begin 
          $display("Inputs: %0d (check condition),%0d (target location)", rt, rs);
          if (rt == 0) begin
            $display("Move taken");
            result = rs;
          end
          DMwe = 0;
          size = 0;
          br = 0;
        end
        6'b001011: begin
          $display("Inputs: %0d (check condition),%0d (target location)", rt, rs);
          if (rt != 0) begin
            $display("Move taken");
            result = rs;
          end
          DMwe = 0;
          size = 0;
          br = 0;
        end
        6'b010000: begin
          $display("Inputs: %0d", HI);
          result = HI;
          DMwe = 0;
          size = 0;
          Rwd  = 1;
          br = 0;
        end
        6'b010010: begin
          $display("Inputs: %0d", LO);
          result = LO;
          DMwe = 0;
          size = 0;
          Rwd  = 1;
          br = 0;
        end
        6'b011010: begin
          $display("Inputs: %0d divided by %0d", rs, rt);
          LO = rs / rt;
          HI = rs % rt;
          result = LO;
          DMwe = 0;
          size = 0;
          Rwd  = 1;
          br = 0;
          $display("Remainder: %0d", HI);
        end
        6'b011011: begin
          $display("Inputs: %0d divided by %0d", rs, rt);
          LO = rs / rt;
          HI = rs % rt;
          result = LO;
          DMwe = 0;
          size = 0;
          Rwd  = 1;
          br = 0;
          $display("Remainder: %0d", HI);
        end
        6'b100000: begin
          $display("Inputs: %0d added with %0d", rs, rt);
          result = rs + rt;
          DMwe = 0;
          size = 0;
          Rwd  = 1;
          br = 0;
        end
        6'b100001: begin
          $display("Inputs: %0d added with %0d", rs, rt);
          result = rs + rt;
          DMwe = 0;
          size = 0;
          Rwd  = 1;
          br = 0;
        end
        6'b100010: begin
          $display("Inputs: %0d subtracted by %0d", rs, rt);
          result = rs - rt;
          DMwe = 0;
          size = 0;
          Rwd  = 1;
          br = 0;
        end
        6'b100011: begin
          $display("Inputs: %0d subtracted by %0d", rs, rt);
          result = rs - rt;
          DMwe = 0;
          size = 0;
          Rwd  = 1;
          br = 0;
        end
        6'b100100: begin
          $display("Inputs: %0d AND %0d", rs, rt);
          result = rs & rt;
          DMwe = 0;
          size = 0;
          Rwd  = 1;
          br = 0;
        end
        6'b100101: begin
          $display("Inputs: %0d OR %0d", rs, rt);
          result = rs | rt;
          DMwe = 0;
          size = 0;
          Rwd  = 1;
          br = 0;
        end
        6'b100110: begin
          $display("Inputs: %0d XOR %0d", rs, rt);
          result = rs ^ rt;
          DMwe = 0;
          size = 0;
          Rwd  = 1;
          br = 0;
        end
        6'b100111: begin
          $display("Inputs: %0d NOR %0d", rs, rt);
          result = ~(rs | rt);
          DMwe = 0;
          size = 0;
          Rwd  = 1;
          br = 0;
        end
        6'b101010: begin
          $display("Inputs: %0d less than %0d", rs, rt);
          if (rs < rt) begin
            $display("TRUE");
            result = 1;
          end else begin
            $display("FALSE");
            result = 0;
          end
          DMwe = 0;
          size = 0;
          Rwd  = 1;
          br = 0;
        end
        6'b101011: begin
          $display("Inputs: %0d less than %0d", rs, rt);
          if (rs < rt) begin
            $display("TRUE");
            result = 1;
          end else begin
            $display("FALSE");
            result = 0;
          end
          DMwe = 0;
          size = 0;
          Rwd  = 1;
          br = 0;
        end
      endcase
      // Special case I-types
      6'b000001: case (rt)
        6'b00000: begin
          $display("Inputs: %0d less than 0", rs);
          if (rs < 0) begin
            signed_ext[0:13]  = {14{immed[0]}};
            signed_ext[14:31] = immed << 2;
            $display("Branch taken, target: %h", pc + signed_ext);
            addr = pc + signed_ext + 4;
            br = 1;
          end else begin
            br = 0;
          end
          DMwe = 0;
        end
        6'b00001: begin
          $display("Inputs: %0d greater than or equal to 0", rs);
          if (rs >= 0) begin
            signed_ext[0:13]  = {14{immed[0]}};
            signed_ext[14:31] = immed << 2;
            $display("Branch taken, target: %h", pc + signed_ext);
            addr = pc + signed_ext + 4;
            br = 1;
          end else begin
            br = 0;
          end
          DMwe = 0;
          size = 0;
        end
      endcase
      // J-types
      6'b000010: begin
        addr[0:3] = pc[0:3];
        addr[4:29] = target;
        addr[30:31] = 2'b00;
        $display("Inputs: %0d (immediate target, extended)", addr);
        DMwe = 0;
        size = 0;
        br   = 0;
      end
      6'b000011: begin
        addr[0:3] = pc[0:3];
        addr[4:29] = target;
        addr[30:31] = 2'b00;
        $display("Inputs: %0d (immediate target, extended)", addr);
        result = pc + 8;
        DMwe = 0;
        size = 0;
        Rwd  = 1;
        br   = 0;
      end
      // I-types
      6'b000100: begin
        $display("Inputs: %0d equals to %0d", rs, rt);
        if (rs == rt) begin
          signed_ext[0:13]  = {14{immed[0]}};
          signed_ext[14:31] = immed << 2;
          $display("Branch taken, target: ", pc + signed_ext);
          addr = pc + signed_ext + 4;
          br   = 1;
        end else begin
          $display("Branch not taken");
          br   = 0;
        end
        DMwe = 0;
        size = 0;
      end
      6'b000101: begin
        $display("Inputs: %0d not equal to %0d", rs, rt);
        if (rs != rt) begin
          signed_ext[0:13]  = {14{immed[0]}};
          signed_ext[14:31] = immed << 2;
          $display("Branch taken, target: ", pc + signed_ext);
          addr = pc + signed_ext + 4;
          br   = 1;
        end else begin
          $display("Branch not taken");
          br   = 0;
        end
        DMwe = 0;
        size = 0;
      end
      6'b000110: begin
        $display("Inputs: %0d less than or equals to 0", rs);
        if (rs <= 0) begin
          signed_ext[0:13]  = {14{immed[0]}};
          signed_ext[14:31] = immed << 2;
          $display("Branch taken, target: ", pc + signed_ext);
          addr = pc + signed_ext + 4;
          br   = 1;
        end else begin
          br   = 0;
        end
        DMwe = 0;
        size = 0;
      end
      6'b000111: begin
        $display("Inputs: %0d greater than or equals to 0", rs);
        if (rs >= 0) begin
          signed_ext[0:13]  = {14{immed[0]}};
          signed_ext[14:31] = immed << 2;
          $display("Branch taken, target: ", pc + signed_ext);
          addr = pc + signed_ext + 4;
          br   = 1;
        end else begin
          br   = 0;
        end
        DMwe = 0;
        size = 0;
      end
      6'b001000: begin
        signed_ext[0:15]  = {16{immed[0]}};
        signed_ext[16:31] = immed;
        $display("Inputs: %0d, added with %0d (sign extended to 32-bits)", rs, signed_ext);
        result = rs + signed_ext;
        DMwe = 0;
        size = 0;
        Rwd  = 1;
      end
      6'b001001: begin
        signed_ext[0:15]  = {16{immed[0]}};
        signed_ext[16:31] = immed;
        $display("Inputs: %0d, added with %0d (sign extended to 32-bits)", rs, signed_ext);
        result = rs + signed_ext;
        DMwe = 0;
        size = 0;
        Rwd  = 1;
      end
      6'b001010: begin
        signed_ext[0:15]  = {16{immed[0]}};
        signed_ext[16:31] = immed;
        $display("Inputs: %0d less than %0d (sign extended to 32-bits)", rs, signed_ext);
        if (rs < signed_ext) begin
          $display("TRUE");
          result = 1;
        end else begin
          $display("FALSE");
          result = 0;
        end
        DMwe = 0;
        size = 0;
        Rwd  = 1;
      end
      6'b001011: begin
        signed_ext[0:15]  = {16{immed[0]}};
        signed_ext[16:31] = immed;
        $display("Inputs: %0d less than %0d (sign extended to 32-bits)", rs, signed_ext);
        if (rs < signed_ext) begin
          $display("TRUE");
          result = 1;
        end else begin
          $display("FALSE");
          result = 0;
        end
        DMwe = 0;
        size = 0;
        Rwd  = 1;
      end
      6'b001100: begin
        signed_ext[0:15]  = {16{immed[0]}};
        signed_ext[16:31] = immed;
        $display("Inputs: %0d AND %0d (sign extended to 32-bits)", rs, signed_ext);
        result = rs & signed_ext;
        DMwe = 0;
        size = 0;
        Rwd  = 1;
      end
      6'b001101: begin
        $display("Inputs: %0d OR %0d (sign extended to 32-bits)", rs, {16'b0,immed});
        result = rs | {16'b0,immed};
        DMwe = 0;
        size = 0;
        Rwd  = 1;
      end
      6'b001110: begin
        $display("Inputs: %0d XOR %0d (sign extended to 32-bits)", rs, {16'b0,immed});
        result = rs ^ {16'b0,immed};
        DMwe = 0;
        size = 0;
        Rwd  = 1;
      end
      6'b001111: begin
        $display("Inputs: Load upper immediate %0d", immed << 16);
        result = immed << 16;
        DMwe = 0;
        size = 0;
        Rwd  = 1;
      end
      6'b011100: case (func)
        6'b000010: begin
          $display("Inputs: %0d multiplied by %0d", rs, rt);
          result = rs * rt;
          DMwe = 0;
          size = 0;
          Rwd  = 1;
        end
      endcase
      6'b100000: begin
        $display("Load byte from 0x%h", {immed[0],16'b0,immed[1:15]} + rs);
        result = {immed[0],16'b0,immed[1:15]} + rs;
        DMwe = 0;
        size = 0;
        Rwd  = 0;
      end
      6'b100001: begin
        $display("Load half-word from 0x%h", {immed[0],16'b0,immed[1:15]} + rs);
        result = {immed[0],16'b0,immed[1:15]} + rs;
        DMwe = 0;
        size = 1;
        Rwd  = 0;
      end
      6'b100011: begin
        $display("Load word from 0x%h", {immed[0],16'b0,immed[1:15]} + rs);
        result = {immed[0],16'b0,immed[1:15]} + rs;
        DMwe = 0;
        size = 2;
        Rwd  = 0;
      end
      6'b100100: begin
        $display("Load unsigned byte from 0x%h", {immed[0],16'b0,immed[1:15]} + rs);
        result = {immed[0],16'b0,immed[1:15]} + rs;
        DMwe = 0;
        size = 3;
        Rwd  = 0;
      end
      6'b101000: begin
        $display("Store byte to 0x%h", {immed[0],16'b0,immed[1:15]} + rs);
        result = {immed[0],16'b0,immed[1:15]} + rs;
        DMwe = 1;
        size = 0;
      end
      6'b101001: begin
        $display("Store half-word to 0x%h", {immed[0],16'b0,immed[1:15]} + rs);
        result = {immed[0],16'b0,immed[1:15]} + rs;
        DMwe = 1;
        size = 1;
      end
      6'b101011: begin
        $display("Store word to 0x%h", {immed[0],16'b0,immed[1:15]} + rs);
        result = {immed[0],16'b0,immed[1:15]} + rs;
        DMwe = 1;
        size = 2;
      end
    endcase
    $display("Result: %0d", result);
  end

endmodule
