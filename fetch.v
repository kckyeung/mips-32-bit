module fetch(clk, stall, target,
             addr, next, rw, access_size);
  input clk;
  input stall;
  input [0:31] target;

  output [0:31] addr;
  output [0:31] next;
  output        rw;
  output [0:1]  access_size;

  reg [0:31] pc;

  assign rw = 1;
  assign access_size = 2;
  assign addr = pc;

  initial begin
    pc = 32'h80020000;
  end

  assign next = pc + 4;

  always @ (posedge clk)
  begin: INC_ADDR
    if (stall !== 1) begin
      pc <= target;
    end
  end
endmodule
