module memory(clk, addr, d_in, access_size, rw, enable,
              d_out, busy);
  parameter FRAMES = 1048576; //This gives us 1 MB of memory

  input        clk;
  input [0:31] addr;
  input [0:31] d_in;
  input [0:1]  access_size;
  input        rw;
  input        enable;

  output reg [0:31] d_out;
  output            busy;

  wire [0:31] mem_idx;
  reg  [0:7]  main_memory [0:FRAMES-1];
  reg  [0:3]  access_cycle;

  initial begin
    $readmemh("SimpleAdd.x", main_memory);
  end

  assign mem_idx = addr - 32'h80020000;
  assign busy = enable;

  always @ (posedge clk)
  begin: MEM_WRITE
    if (enable && !rw) begin
      main_memory[mem_idx] <= d_in[0:7];
      if (access_size > 0) begin
        main_memory[mem_idx+1] <= d_in[8:15];
        if (access_size > 1) begin
          main_memory[mem_idx+2] <= d_in[16:23];
          main_memory[mem_idx+3] <= d_in[24:31];
        end
      end
    end
  end

  always @ (posedge clk)
  begin: MEM_READ
    if (enable && rw) begin
      if (access_size == 0) begin
        d_out[0:23]  <= {24{main_memory[mem_idx][0]}};
        d_out[24:31] <= main_memory[mem_idx];
      end else if (access_size == 1) begin
        d_out[0:15]  <= {16{main_memory[mem_idx][0]}};
        d_out[16:23] <= main_memory[mem_idx];
        d_out[23:31] <= main_memory[mem_idx+1];
      end else if (access_size == 2) begin
        d_out[0:7]   <= main_memory[mem_idx];
        d_out[8:15]  <= main_memory[mem_idx+1];
        d_out[16:23] <= main_memory[mem_idx+2];
        d_out[24:31] <= main_memory[mem_idx+3];
      end else if (access_size == 3) begin
        d_out[0:23]  <= 24'h0;
        d_out[24:31] <= main_memory[mem_idx];
      end
    end
  end
endmodule
