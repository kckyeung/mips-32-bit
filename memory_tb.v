module pd1_tb();
     
    reg  clk;
    reg [0:31]addr; 
    reg [0:31]d_in; 
    reg [0:1]access_size; 
    reg rw; 
    reg enable;
              
    wire [0:31] d_out; 
    wire busy; 
        
    initial 
    begin 
        $display ("clk addr\t  d_in access_size rw enable d_out busy"); 
        $monitor ("%b   %h     %h      %h   %b    %b    %h    %b", 
            clk, addr, d_in, access_size, rw, enable, d_out, busy);
        clk = 1; 
        addr = 32'h80020000; 
        d_in = 32'b0;
        access_size = 2'h0;
        rw = 1;
        enable = 0;

    #10 enable = 1;
    #10 addr = 32'h80020004;
        access_size = 2'h1;
    #20 addr = 32'h8002000C;
        access_size = 2'h2;
    #40 addr = 32'h8002001C;
        access_size = 2'h3;
    end 
      
    always 
       #5  clk =  ! clk; 

    memory main(clk, addr, d_in, access_size, rw, enable, d_out, busy);  
endmodule