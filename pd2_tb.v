module pd2_tb();
    reg clk;
    reg stall;

    wire [0:31] addr;
    wire [0:31] insn;
    wire [0:1]  access_size;
    wire        rw;
    wire        busy;

    initial 
    begin
        clk = 1;
        stall = 0;
    end 
      
    always 
       #5  clk =  ! clk; 
    fetch  program_counter(clk, stall, addr, rw, access_size);
    memory main(clk, addr, 32'b0, access_size, rw, 1'b1, insn, busy);
    decode mips_dec(clk, insn, addr);
endmodule
