module pd3_tb();
  reg clk;

  reg [0:31] fd_addr;
  reg [0:31] dx_insn;
  reg [0:31] dx_addr;
  reg [0:31] xm_dval;
  reg [0:31] mw_dval;
  reg [0:31] wb_dval;
  reg [0:4]  xm_rdst;
  reg [0:4]  mw_rdst;
  reg [0:3]  xm_size;
  reg        xm_dmwe;
  reg        xm_rwe;
  reg        xm_rwd;
  reg        mw_rwd;
  reg        mw_rwe;

  wire [0:31] inc_addr;
  wire [0:31] next_addr;
  wire [0:31] fm_addr;
  wire [0:31] xm_rt;
  wire [0:31] addr;
  wire [0:31] pc;
  wire [0:31] insn;
  wire [0:31] rsOut;
  wire [0:31] rtOut;
  wire [0:31] Rdst;
  wire [0:31] dval;
  wire [0:31] rval;
  wire [0:31] jmp_tgt;
  wire [0:31] br_tgt;
  wire [0:31] DMout;
  wire [0:1]  access_size;
  wire [0:1]  size;
  wire [0:1]  mx_bypass;
  wire [0:1]  wx_bypass;
  wire        jmp;
  wire        br;
  wire        rw;
  wire        DMwe;
  wire        Rwd;
  wire        Rwe;
  wire        busy;
  wire        stall;

  initial
  begin
    clk = 1;
  end

  always @ (posedge clk)
  begin
    fd_addr <= fm_addr;
    if (stall !== 1) begin
      dx_addr <= fd_addr;
      dx_insn <= insn;
      xm_rwe  <= Rwe;
      xm_rdst <= Rdst;
    end else begin
      dx_addr <= 32'h0;
      dx_insn <= 32'h0;
      xm_rwe  <= 0;
      xm_rdst <= 5'h0;
    end
    xm_dmwe <= DMwe;
    xm_dval <= dval;
    xm_rwd  <= Rwd;
    xm_size <= size;
    mw_dval <= xm_dval;
    mw_rwd  <= xm_rwd;
    mw_rdst <= xm_rdst;
    mw_rwe  <= xm_rwe;
    wb_dval <= rval;
  end

  assign fm_addr = (stall !== 1 && br !== 1) ? addr : addr - 4;

  always
    #5 clk = !clk;
  fetch program_counter(clk, stall, next_addr, addr, inc_addr, rw, access_size);
  memory insn_mem(clk, fm_addr, 32'b0, access_size, rw, 1, insn, busy);
  decode mips_dec(clk, insn, fd_addr, rval, mw_rdst, mw_rwe, rsOut, rtOut, jmp_tgt, Rdst, Rwe, jmp, stall, mx_bypass, wx_bypass);
  execute alu(clk, rsOut, rtOut, dx_addr, dx_insn, xm_dval, rval, mx_bypass, wx_bypass, xm_rt, dval, br_tgt, size, DMwe, Rwd, br);
  memory data_mem(clk, xm_dval, xm_rt, xm_size, !xm_dmwe, 1, DMout, busy);
  writeback wb(mw_rwd, jmp, br, mw_dval, DMout, jmp_tgt, br_tgt, inc_addr, rval, next_addr);
endmodule
