module register(clk, we, dval, s1, s2, d, 
                s1val, s2val);
  input clk;
  input we;
  input [0:31] dval;
  input [0:4]  s1;
  input [0:4]  s2;
  input [0:4]  d;

  output reg [0:31] s1val;
  output reg [0:31] s2val;

  reg [0:31] r0;
  reg [0:31] r1;
  reg [0:31] r2;
  reg [0:31] r3;
  reg [0:31] r4;
  reg [0:31] r5;
  reg [0:31] r6;
  reg [0:31] r7;
  reg [0:31] r8;
  reg [0:31] r9;
  reg [0:31] r10;
  reg [0:31] r11;
  reg [0:31] r12;
  reg [0:31] r13;
  reg [0:31] r14;
  reg [0:31] r15;
  reg [0:31] r16;
  reg [0:31] r17;
  reg [0:31] r18;
  reg [0:31] r19;
  reg [0:31] r20;
  reg [0:31] r21;
  reg [0:31] r22;
  reg [0:31] r23;
  reg [0:31] r24;
  reg [0:31] r25;
  reg [0:31] r26;
  reg [0:31] r27;
  reg [0:31] r28;
  reg [0:31] r29;
  reg [0:31] r30;
  reg [0:31] r31;

  initial
  begin
    r0 =  32'h0;
    r1 =  32'h1;
    r2 =  32'h2;
    r3 =  32'h3;
    r4 =  32'h4;
    r5 =  32'h5;
    r6 =  32'h6;
    r7 =  32'h7;
    r8 =  32'h8;
    r9 =  32'h9;
    r10 = 32'ha;
    r11 = 32'hb;
    r12 = 32'hc;
    r13 = 32'hd;
    r14 = 32'he;
    r15 = 32'hf;
    r16 = 32'h10;
    r17 = 32'h11;
    r18 = 32'h12;
    r19 = 32'h13;
    r20 = 32'h14;
    r21 = 32'h15;
    r22 = 32'h16;
    r23 = 32'h17;
    r24 = 32'h18;
    r25 = 32'h19;
    r26 = 32'h1a;
    r27 = 32'h1b;
    r28 = 32'h1c;
    r29 = 32'h80120000;
    r30 = 32'h1e;
    r31 = 32'hdeadbeef;
  end

  always @ (posedge clk)
  begin: WRITE_DATA
    if (we) begin
      case (d)
        // Ignore writes to r0
        5'h01: r1  <= dval;
        5'h02: r2  <= dval;
        5'h03: r3  <= dval;
        5'h04: r4  <= dval;
        5'h05: r5  <= dval;
        5'h06: r6  <= dval;
        5'h07: r7  <= dval;
        5'h08: r8  <= dval;
        5'h09: r9  <= dval;
        5'h0a: r10 <= dval;
        5'h0b: r11 <= dval;
        5'h0c: r12 <= dval;
        5'h0d: r13 <= dval;
        5'h0e: r14 <= dval;
        5'h0f: r15 <= dval;
        5'h10: r16 <= dval;
        5'h11: r17 <= dval;
        5'h12: r18 <= dval;
        5'h13: r19 <= dval;
        5'h14: r20 <= dval;
        5'h15: r21 <= dval;
        5'h16: r22 <= dval;
        5'h17: r23 <= dval;
        5'h18: r24 <= dval;
        5'h19: r25 <= dval;
        5'h1a: r26 <= dval;
        5'h1b: r27 <= dval;
        5'h1c: r28 <= dval;
        5'h1d: r29 <= dval;
        5'h1e: r30 <= dval;
        5'h1f: r31 <= dval;
      endcase
    end
  end

  always @ (negedge clk)
  begin: DUMP_DATA
    if (we) begin
      $display("R0:  %h", r0);
      $display("R1:  %h", r1);
      $display("R2:  %h", r2);
      $display("R3:  %h", r3);
      $display("R4:  %h", r4);
      $display("R5:  %h", r5);
      $display("R6:  %h", r6);
      $display("R7:  %h", r7);
      $display("R8:  %h", r8);
      $display("R9:  %h", r9);
      $display("R10: %h", r10);
      $display("R11: %h", r11);
      $display("R12: %h", r12);
      $display("R13: %h", r13);
      $display("R14: %h", r14);
      $display("R15: %h", r15);
      $display("R16: %h", r16);
      $display("R17: %h", r17);
      $display("R18: %h", r18);
      $display("R19: %h", r19);
      $display("R20: %h", r20);
      $display("R21: %h", r21);
      $display("R22: %h", r22);
      $display("R23: %h", r23);
      $display("R24: %h", r24);
      $display("R25: %h", r25);
      $display("R26: %h", r26);
      $display("R27: %h", r27);
      $display("R28: %h", r28);
      $display("R29: %h", r29);
      $display("R30: %h", r30);
      $display("R31: %h", r31);
    end
  end

  always @ (posedge clk)
  begin: READ_DATA
    if (we === 1 && s1 == d) begin
      s1val <= dval;
    end else begin
    case (s1)
      5'h00: s1val <= r0;
      5'h01: s1val <= r1;
      5'h02: s1val <= r2;
      5'h03: s1val <= r3;
      5'h04: s1val <= r4;
      5'h05: s1val <= r5;
      5'h06: s1val <= r6;
      5'h07: s1val <= r7;
      5'h08: s1val <= r8;
      5'h09: s1val <= r9;
      5'h0a: s1val <= r10;
      5'h0b: s1val <= r11;
      5'h0c: s1val <= r12;
      5'h0d: s1val <= r13;
      5'h0e: s1val <= r14;
      5'h0f: s1val <= r15;
      5'h10: s1val <= r16;
      5'h11: s1val <= r17;
      5'h12: s1val <= r18;
      5'h13: s1val <= r19;
      5'h14: s1val <= r20;
      5'h15: s1val <= r21;
      5'h16: s1val <= r22;
      5'h17: s1val <= r23;
      5'h18: s1val <= r24;
      5'h19: s1val <= r25;
      5'h1a: s1val <= r26;
      5'h1b: s1val <= r27;
      5'h1c: s1val <= r28;
      5'h1d: s1val <= r29;
      5'h1e: s1val <= r30;
      5'h1f: s1val <= r31;
    endcase
    end
    if (we === 1 && s2 == d) begin
      s2val <= dval;
    end else begin
    case (s2)
      5'h00: s2val <= r0;
      5'h01: s2val <= r1;
      5'h02: s2val <= r2;
      5'h03: s2val <= r3;
      5'h04: s2val <= r4;
      5'h05: s2val <= r5;
      5'h06: s2val <= r6;
      5'h07: s2val <= r7;
      5'h08: s2val <= r8;
      5'h09: s2val <= r9;
      5'h0a: s2val <= r10;
      5'h0b: s2val <= r11;
      5'h0c: s2val <= r12;
      5'h0d: s2val <= r13;
      5'h0e: s2val <= r14;
      5'h0f: s2val <= r15;
      5'h10: s2val <= r16;
      5'h11: s2val <= r17;
      5'h12: s2val <= r18;
      5'h13: s2val <= r19;
      5'h14: s2val <= r20;
      5'h15: s2val <= r21;
      5'h16: s2val <= r22;
      5'h17: s2val <= r23;
      5'h18: s2val <= r24;
      5'h19: s2val <= r25;
      5'h1a: s2val <= r26;
      5'h1b: s2val <= r27;
      5'h1c: s2val <= r28;
      5'h1d: s2val <= r29;
      5'h1e: s2val <= r30;
      5'h1f: s2val <= r31;
    endcase
    end
  end

endmodule
